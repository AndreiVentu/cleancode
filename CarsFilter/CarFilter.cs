﻿using System.Collections.Generic;
using System.Linq;
using CarsFilter.Enums;

namespace CarsFilter
{
    public class CarFilter
    {
        public List<Car> FilterByType(IEnumerable<Car> cars, CarType type) =>
            cars.Where(c => c.Type == type).ToList();

        public List<Car> FilterByColor(IEnumerable<Car> cars, Color color) =>
            cars.Where(c => c.Color == color).ToList();

        public List<Car> FilterByTypeAndColor(IEnumerable<Car> cars, CarType type,Color color) =>
            cars.Where(c =>
            {
                return c.Type == type && c.Color==color;
            }).ToList();

    }
}
