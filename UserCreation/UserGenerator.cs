﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserCreation
{
    class UserGenerator
    {
        public static Person UserGenerate(List<string> personData)
        {
            Person p = new Person();
            p.FirstName = personData[0];
            p.LastName = personData[1];
            Messages.ShowUser(p);
            return p;
        }
    }
}
