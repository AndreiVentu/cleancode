﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserCreation
{
    class Messages
    {
        public static void Welcome()
        {
            Console.WriteLine("Welcome to user creation app!");
        }

        public static void ErrorMsg(string msg)
        {
            Console.WriteLine(msg);
        }

        public static void ShowUser(Person user)
        {
            Console.WriteLine($"Your new username is {user.FirstName}{" "}{user.LastName}");
        }

        public static void ExitMessage()
        {
            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();
        }
    }
}
