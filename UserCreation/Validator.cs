﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserCreation
{
    class Validator
    {
        public static bool validate(List<string> personData)
        {
           
            if (string.IsNullOrWhiteSpace(personData[0]))
            {
                Messages.ErrorMsg("First name is not valid!");
                return false;
            }

            if (string.IsNullOrWhiteSpace(personData[1]))
            {
                Messages.ErrorMsg("Last name is not valid!");
                return false;
            }
            return true;
        }
    }
}
