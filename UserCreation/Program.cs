﻿using System;
using System.Collections.Generic;

namespace UserCreation
{
    class Program
    {
        static void Main(string[] args)
        {
            Messages.Welcome();
            List<string> PersonData = new List<string>();
            Person User = new Person();
            if (Validator.validate(PersonData = UserDataInput.PersonDataInput()) == true)
            {
                User = UserGenerator.UserGenerate(PersonData);
            }

            Messages.ExitMessage();
        }
    }
}
