﻿using PhoneFactory.Phones;
using System;

namespace PhoneFactory
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var p30 = new HuaweiP30();
            p30.Id = 123;
            p30.Name = "HuaweiP30";
            p30.PowerOn();
            p30.AccessInternet();
            p30.ConnectToDeviceViaNFC("TV");
            p30.MakeCall(0272727272);
            p30.PlayMP3("song.mp3");
            p30.ShareFileOverBluetooth("file.txt");
            Console.WriteLine(p30.GetInformation());
            p30.PowerOff();

            var nokia3310 = new Nokia3310();
            nokia3310.Id = 124;
            nokia3310.Name = "Nokia3310";
            nokia3310.PowerOn();
            nokia3310.MakeCall(0272727272);
            Console.WriteLine(nokia3310.GetInformation());
            nokia3310.PowerOff();

            var Alcatel = new Alcatel();
            Alcatel.Id = 124;
            Alcatel.Name = "Alcatel";
            Alcatel.PowerOn();
            Alcatel.MakeCall(0272727272);
            Console.WriteLine(Alcatel.GetInformation());
            Alcatel.PowerOff();

        }
    }
}