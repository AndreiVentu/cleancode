﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneFactory.Interfaces
{
    public interface IMusicAndInternet
    {
        void AccessInternet();
        void PlayMP3(string songName);
    }
}
