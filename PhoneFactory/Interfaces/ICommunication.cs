﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneFactory.Interfaces
{
    public interface ICommunication
    {
        void ConnectToDeviceViaNFC(string deviceName);
        void ShareFileOverBluetooth(string fileName);
    }
}
