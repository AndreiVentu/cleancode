﻿namespace SmartHouse
{
    public class House
    {
        private readonly Security _security;
        private readonly InformationService _informationService;

        public House(Security s,FileDatabase db)
        {
            _security = s;
            var fileDatabase = db;
            _informationService = new InformationService(fileDatabase);
        }

        public void EnableFullLockDown()
        {
            _security.FullLockDown();
        }

        public void LockMainDoor()
        {
            _security.LockFrontDoor();
        }

        public string UnlockMainDoor(decimal securityCode)
        {
            

            return _security.UnlockFrontDoor(securityCode);
        }

        public string GetHouseHistory()
        {
            return _informationService.GetInformation();
        }
    }
}