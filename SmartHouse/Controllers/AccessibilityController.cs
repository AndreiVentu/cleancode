﻿using Microsoft.AspNetCore.Mvc;

namespace SmartHouse.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccessibilityController : ControllerBase
    {
        House house{ get; set; }
        public AccessibilityController(House house)
        {
            this.house = house;
        }

        [HttpGet("GetHouseHistory")]
        public string GetHouseHistory()
        {
            return house.GetHouseHistory();
        }

        [HttpPost("LockDownTheHouse")]
        public void LockDownTheHouse()
        {
        

            house.EnableFullLockDown();
        }

        [HttpPost("LockMainDoor")]
        public void LockMainDoor()
        {
      

            house.LockMainDoor();
        }

        [HttpPost("UnlockMainDoor")]
        public string UnlockMainDoor(decimal securityCode)
        {
  

            return house.UnlockMainDoor(securityCode);
        }
    }
}