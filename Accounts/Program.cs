﻿using Accounts.AccountTypes;

namespace Accounts
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            SavingAccount account1 = new RegularSavingAccount();
            SavingAccount account2 = new SalarySavingAccount();
            //SavingAccount account3 = new FixDepositSavingAccount();

            AccountManager.WithdrawFrom(account1, 100);
            AccountManager.WithdrawFrom(account2, 200);
            AccountManager.WithdrawFrom(account3, 300);

            //Din cate am vazut, se cere sa obtinem un compile time error asa ca am utilizat account3 fara sa il declar
            //Acum nu stiu sigur daca asta trebuie de fapt de facut tinand cont ca tema este legata de clean code
            //Poate trebuia sa organizam mai bine clasele folosind si interfete
        }
    }
}